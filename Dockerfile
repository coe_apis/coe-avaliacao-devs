FROM library/eclipse-temurin:17.0.9_9-jdk

WORKDIR /app

COPY target/  /app/

ENV TZ=America/Sao_Paulo
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone


ENTRYPOINT ["java", "-XX:+UseParallelGC", "-XX:ParallelGCThreads=2","-Xms512m", "-jar", "/app/app.jar"]

