# CRM - TelCom Innovate

Este projeto é um sistema de gerenciamento de clientes. A aplicação permite a criação, atualização, exclusão e listagem de clientes, além de integrar-se a um serviço externo para obter informações de CEP.

## Funcionalidades

### Gerenciamento de Clientes

- **Criação de Clientes**: Permite criar novos clientes com validação de CPF e CEP. Um erro é lançado se o CPF já existir no sistema.
- **Atualização de Clientes**: Atualiza as informações dos clientes, com validações de CPF e CEP.
- **Exclusão de Clientes**: Remove clientes do sistema.
- **Listagem de Clientes**: Exibe todos os clientes cadastrados.
- **Busca por ID**: Permite a busca de um cliente específico pelo seu ID.

### Integração com Serviço Externo de CEP

- **Busca de CEP**: Quando um CEP não é encontrado no repositório local, a aplicação consulta um serviço externo (ViaCep) para obter as informações.
- **Armazenamento de CEP**: As informações obtidas externamente são armazenadas localmente para futuras consultas.

### Validações

- **Validação de CPF**: Verifica se o CPF fornecido é válido e se já existe no sistema.
- **Validação de CEP**: Confere se o CEP fornecido é válido e busca as informações correspondentes.

### Tratamento de Exceções

- Utiliza exceções HTTP para indicar erros, como CPF inválido, cliente não encontrado, e conflitos de CPF já existente.

## Componentes Principais

### Serviços

- `ClientService`: Gerencia operações relacionadas aos clientes.
- `CEPService`: Gerencia operações relacionadas aos CEPs.

### Repositórios

- `ClientRepository`: Interface para operações de banco de dados relacionadas aos clientes.
- `CepRepository`: Interface para operações de banco de dados relacionadas aos CEPs.

### Entidades

- `Client`: Representa a entidade Cliente.
- `Cep`: Representa a entidade CEP.

### Clientes Externos

- `ViaCepClient`: Cliente para interagir com o serviço externo ViaCep.

## Testes

- **Testes Unitários**: Incluem testes para os serviços de clientes (`ClientServiceTest`) e CEPs (`CEPServiceTest`).

## Desenvolvimento

### GitFlow

Para o versionamento adequado, siga o fluxo de trabalho detalhado em [gitflow.md](Documents/gitflow.md). As etapas principais incluem:

### Roteiro da Prova Prática

Para a realização da prova prática, um roteiro detalhado pode ser encontrado em [roteiro_da_prova_v3.md](Documents/Roteiro_da_Prova_v3.md). Este roteiro abrange todas as etapas da prova, desde a configuração inicial do ambiente até o teste de estresse e ajustes finais.

