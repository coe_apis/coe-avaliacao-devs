package br.com.innovate.crm.resource;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import br.com.innovate.crm.model.ClientRequest;
import br.com.innovate.crm.model.ClientResponse;
import br.com.innovate.crm.service.ClientService;

import java.util.Arrays;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
public class ClientResourceTest {

        private MockMvc mockMvc;

        @InjectMocks
        private ClientResource clientResource;

        @Mock
        private ClientService clientService;

        private ClientResponse client;

        @BeforeEach
        void setUp() {

                client = ClientResponse.builder().name("Teste").email("teste@teste.com").cpf("12345678909").build();

                mockMvc = MockMvcBuilders.standaloneSetup(clientResource).build();
        }

        @Test
        @DisplayName("Should delete a client when DELETE request is made with a valid ID")
        void whenDeleteIsCalledWithValidIdThenClientIsDeleted() throws Exception {
                mockMvc.perform(delete("/client/1")
                                .contentType(MediaType.APPLICATION_JSON))
                                .andExpect(status().isNoContent());
        }

        @Test
        @DisplayName("Should return list of clients when GET request is made to /client")
        void whenGetIsCalledThenListOfClientsIsReturned() throws Exception {
                when(clientService.getAllClients())
                                .thenReturn(Arrays.asList(client));

                mockMvc.perform(get("/client")
                                .contentType(MediaType.APPLICATION_JSON))
                                .andExpect(status().isOk())
                                .andExpect(jsonPath("$[0].name").value("Teste"))
                                .andExpect(jsonPath("$[0].email").value("teste@teste.com"));
                // .andExpect(jsonPath("$[0].clientCep.cep").value("12345678"));
        }

        @Test
        @DisplayName("Should create a new client when POST request is successful")
        void whenPostIsCalledShouldCreateClient() throws Exception {
                when(clientService.saveClientWithCep(any(ClientRequest.class)))
                                .thenReturn(client);

                mockMvc.perform(post("/client")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("{\"name\":\"Teste\",\"email\":\"teste@teste.com\",\"clientCep\":{\"cep\":\"12345678\"}}"))
                                .andExpect(status().isOk())
                                .andExpect(jsonPath("$.name").value("Teste"))
                                .andExpect(jsonPath("$.email").value("teste@teste.com"));
                // .andExpect(jsonPath("$.clientCep.cep").value("12345678"));
        }

        @Test
        @DisplayName("Should return OK status when GET request is made with a valid ID")
        void whenGetIsCalledWithValidIdThenOkStatusIsReturned() throws Exception {
                when(clientService.getClientById(1L)).thenReturn(client);

                mockMvc.perform(get("/client/1")
                                .contentType(MediaType.APPLICATION_JSON))
                                .andExpect(status().isOk())
                                .andExpect(jsonPath("$.name").value("Teste"))
                                .andExpect(jsonPath("$.email").value("teste@teste.com"));
                // .andExpect(jsonPath("$.clientCep.cep").value("12345678"));
        }
}