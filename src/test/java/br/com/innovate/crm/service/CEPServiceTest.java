package br.com.innovate.crm.service;
import br.com.innovate.crm.clientviacep.ViaCepClient;
import br.com.innovate.crm.entity.Cep;
import br.com.innovate.crm.repository.CepRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.server.ResponseStatusException;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class CEPServiceTest {

    @Mock
    private CepRepository cepRepository;

    @Mock
    private ViaCepClient viaCepClient;

    @InjectMocks
    private CEPService cepService;

    private Cep cep;

    @BeforeEach
    void setUp() {
        cep = new Cep("12345-678", "Rua Teste", "Complemento Teste", "Bairro Teste", "Cidade Teste", "UF", "1234567", "GIA", "DDD", "SIAFI");
    }

    @Test
    @DisplayName("Should throw exception for invalid CEP format")
    void shouldThrowExceptionForInvalidCepFormat() {
        String invalidCep = "12345678";
        ResponseStatusException exception = assertThrows(
                ResponseStatusException.class,
                () -> cepService.getOrCreateCep(invalidCep)
        );

        assertEquals("422 UNPROCESSABLE_ENTITY \"Invalid CEP\"", exception.getMessage());
        verify(cepRepository, never()).findById(anyString());
        verify(viaCepClient, never()).getCep(anyString());
    }

    @Test
    @DisplayName("Should return existing CEP from repository")
    void shouldReturnExistingCepFromRepository() {
        when(cepRepository.findById(cep.getCep())).thenReturn(Optional.of(cep));

        Cep result = cepService.getOrCreateCep(cep.getCep());

        assertNotNull(result);
        assertEquals(cep, result);
        verify(cepRepository, times(1)).findById(cep.getCep());
        verify(viaCepClient, never()).getCep(anyString());
    }

    @Test
    @DisplayName("Should fetch and save CEP when not found in repository")
    void shouldFetchAndSaveCepWhenNotFoundInRepository() {
        when(cepRepository.findById(cep.getCep())).thenReturn(Optional.empty());
        when(viaCepClient.getCep(cep.getCep())).thenReturn(cep);
        when(cepRepository.save(cep)).thenReturn(cep);

        Cep result = cepService.getOrCreateCep(cep.getCep());

        assertNotNull(result);
        assertEquals(cep, result);
        verify(cepRepository, times(1)).findById(cep.getCep());
        verify(viaCepClient, times(1)).getCep(cep.getCep());
        verify(cepRepository, times(1)).save(cep);
    }
}