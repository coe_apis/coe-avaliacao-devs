package br.com.innovate.crm.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import br.com.innovate.crm.clientviacep.ViaCepClient;
import br.com.innovate.crm.entity.Cep;
import br.com.innovate.crm.entity.Client;
import br.com.innovate.crm.model.ClientRequest;
import br.com.innovate.crm.model.ClientResponse;
import br.com.innovate.crm.repository.CepRepository;
import br.com.innovate.crm.repository.ClientRepository;
import br.com.innovate.crm.utils.Validators;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ClientServiceTest {

    @Mock
    private ViaCepClient viaCepClient;

    @Mock
    private CepRepository cepRepository;

    @Mock
    private ClientRepository clientRepository;

    @Mock
    CEPService cepService;

    @InjectMocks
    ClientService clientService;

    private Client client;
    private Cep cep;

    @BeforeEach
    void setUp() {
        cep = new Cep("12345678", "Rua Teste", "Complemento Teste", "Bairro Teste", "Cidade Teste", "UF", "1234567",
                "GIA", "DDD", "SIAFI");
        client = new Client("Teste", "teste@teste.com", cep, null);
    }

    @Test
    @DisplayName("Should deleteClientById ")
    void deleteClientByIdTest() {
        Long clientId = 1L;

        clientService.deleteClientById(clientId);

        verify(clientRepository, times(1)).deleteById(clientId);
    }

    @Test
    @DisplayName("Should save client with valid CPF and non-existing CEP")
    void shouldSaveClientWithValidCpfAndNonExistingCep() {
        ClientRequest clientRequest = new ClientRequest("Teste", "teste@teste.com", "12345678", "12345678909");

        var expectedCep = new Cep("12345678909", "Rua Teste", "Complemento Teste", "Bairro Teste", "Cidade Teste", "UF",
                "1234567", "GIA", "DDD", "SIAFI");

        try (MockedStatic<Validators> mockedValidators = mockStatic(Validators.class)) {
            mockedValidators.when(() -> Validators.isCPFValid(clientRequest.getCpf())).thenReturn(true);
            when(clientRepository.findByCpf(clientRequest.getCpf())).thenReturn(null);
            when(cepService.getOrCreateCep(clientRequest.getCep())).thenReturn(cep);
            when(clientRepository.save(any(Client.class))).thenReturn(client);

            ClientResponse response = clientService.saveClientWithCep(clientRequest);

            assertNotNull(response);
            assertEquals(clientRequest.getName(), response.getName());
            assertEquals(clientRequest.getEmail(), response.getEmail());
            assertEquals(clientRequest.getCep(), expectedCep.getCep());
            verify(clientRepository, times(1)).findByCpf(clientRequest.getCpf());
            verify(cepService, times(1)).getOrCreateCep(clientRequest.getCep());
            verify(clientRepository, times(1)).save(any(Client.class));
        }
    }

    @Test
    @DisplayName("Should throw exception when saving client with invalid CPF")
    void shouldThrowExceptionWhenSavingClientWithInvalidCpf() {
        ClientRequest clientRequest = new ClientRequest("Teste", "teste@teste.com", "12345678", "invalid_cpf");

        try (MockedStatic<Validators> mockedValidators = mockStatic(Validators.class)) {
            mockedValidators.when(() -> Validators.isCPFValid(clientRequest.getCpf())).thenReturn(false);

            ResponseStatusException exception = assertThrows(
                    ResponseStatusException.class,
                    () -> clientService.saveClientWithCep(clientRequest));

            assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, exception.getStatusCode());
            assertEquals("Invalid CPF", exception.getReason());
            verify(clientRepository, never()).findByCpf(anyString());
            verify(cepService, never()).getOrCreateCep(anyString());
            verify(clientRepository, never()).save(any(Client.class));
        }
    }

    @Test
    @DisplayName("Should throw exception when saving client with existing CPF")
    void shouldThrowExceptionWhenSavingClientWithExistingCpf() {
        ClientRequest clientRequest = new ClientRequest("Teste", "teste@teste.com", "12345678", "12345678909");

        try (MockedStatic<Validators> mockedValidators = mockStatic(Validators.class)) {
            mockedValidators.when(() -> Validators.isCPFValid(clientRequest.getCpf())).thenReturn(true);
            when(clientRepository.findByCpf(clientRequest.getCpf())).thenReturn(client);

            ResponseStatusException exception = assertThrows(
                    ResponseStatusException.class,
                    () -> clientService.saveClientWithCep(clientRequest));

            assertEquals(HttpStatus.CONFLICT, exception.getStatusCode());
            assertEquals("Client with CPF already exists", exception.getReason());
            verify(clientRepository, times(1)).findByCpf(clientRequest.getCpf());
            verify(cepService, never()).getOrCreateCep(anyString());
            verify(clientRepository, never()).save(any(Client.class));
        }
    }

    @Test
    @DisplayName("Should get client by ID")
    void shouldGetClientById() {
        Long clientId = 1L;
        when(clientRepository.findById(clientId)).thenReturn(Optional.of(client));

        ClientResponse response = clientService.getClientById(clientId);

        assertNotNull(response);
        assertEquals(client.getName(), response.getName());
        assertEquals(client.getEmail(), response.getEmail());
        verify(clientRepository, times(1)).findById(clientId);
    }

    @Test
    @DisplayName("Should throw exception when client ID not found")
    void shouldThrowExceptionWhenClientIdNotFound() {
        Long clientId = 1L;
        when(clientRepository.findById(clientId)).thenReturn(Optional.empty());

        ResponseStatusException exception = assertThrows(
                ResponseStatusException.class,
                () -> clientService.getClientById(clientId));

        assertEquals(HttpStatus.NOT_FOUND, exception.getStatusCode());
        assertEquals("Client not found", exception.getReason());
        verify(clientRepository, times(1)).findById(clientId);
    }

   

    @Test
    @DisplayName("Should list all clients")
    void shouldListAllClients() {
        when(clientRepository.findAll()).thenReturn(List.of(client));

   
        List<ClientResponse> response = clientService.getAllClients();

        assertNotNull(response);
        assertFalse(response.isEmpty());
        assertEquals(1, response.size());
        assertEquals(client.getName(), response.get(0).getName());
        assertEquals(client.getEmail(), response.get(0).getEmail());
        verify(clientRepository, times(1)).findAll();
    }

    
}