package br.com.innovate.crm;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;


@SpringBootTest
@ActiveProfiles("test")
class CRMApplicationTests {

	@Test
	void contextLoads() {
	}

}
