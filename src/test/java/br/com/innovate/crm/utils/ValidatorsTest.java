package br.com.innovate.crm.utils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;




public class ValidatorsTest {

    @Test
    @DisplayName("Should return true for valid CPF")
    void shouldReturnTrueForValidCpf() {
        String validCpf = "12345678909"; // Replace with a valid CPF for testing
        assertTrue(Validators.isCPFValid(validCpf));
    }

    @Test
    @DisplayName("Should return false for CPF with less than 11 digits")
    void shouldReturnFalseForCpfWithLessThan11Digits() {
        String invalidCpf = "123456789";
        assertFalse(Validators.isCPFValid(invalidCpf));
    }

    @Test
    @DisplayName("Should return false for CPF with more than 11 digits")
    void shouldReturnFalseForCpfWithMoreThan11Digits() {
        String invalidCpf = "123456789012";
        assertFalse(Validators.isCPFValid(invalidCpf));
    }

    @Test
    @DisplayName("Should return false for CPF with all digits the same")
    void shouldReturnFalseForCpfWithAllDigitsTheSame() {
        String invalidCpf = "11111111111";
        assertFalse(Validators.isCPFValid(invalidCpf));
    }

    @Test
    @DisplayName("Should return false for CPF with invalid check digits")
    void shouldReturnFalseForCpfWithInvalidCheckDigits() {
        String invalidCpf = "12345678900"; // Replace with a CPF having invalid check digits
        assertFalse(Validators.isCPFValid(invalidCpf));
    }

    @Test
    @DisplayName("Should return false for CPF with non-numeric characters")
    void shouldReturnFalseForCpfWithNonNumericCharacters() {
        String invalidCpf = "123.456.789-ab";
        assertFalse(Validators.isCPFValid(invalidCpf));
    }
}