package br.com.innovate.crm.resource;

import lombok.extern.slf4j.Slf4j;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import br.com.innovate.crm.model.ClientRequest;
import br.com.innovate.crm.model.ClientResponse;
import br.com.innovate.crm.service.ClientService;

import java.util.List;


/**
 * The ClientResource class is a REST controller that handles HTTP requests for the Client entity.
 * It provides endpoints for creating and retrieving clients.
 *
 * <p>The base URL for these endpoints is "/client".</p>
 *
 * <p>Usage example:</p>
 * <pre>{@code
 * @RestController
 * @RequestMapping(value = "/client")
 * public class ClientResource {
 *     // Endpoint methods
 * }
 * }</pre>
 */

@RestController
@RequestMapping(value = "/client")
@Slf4j
public class ClientResource {
    

    public static final String CORRELATION_ID_HEADER = "X-Correlation-Id";
    private final ClientService clientService;

    public ClientResource(ClientService clientService) {
        this.clientService = clientService;
    }


    @PostMapping
    public ResponseEntity<ClientResponse> createClient(@RequestBody ClientRequest client,
                                               @RequestHeader(value = CORRELATION_ID_HEADER, required = false) String correlationId) throws Exception {
        log.info("Creating client with name: {}", client.getName());
        ClientResponse savedClient = clientService.saveClientWithCep(client);
        return ResponseEntity.ok().body(savedClient);
    }

    @GetMapping
    public ResponseEntity<List<ClientResponse>> buscarTodos(@RequestHeader(value = CORRELATION_ID_HEADER, required = false) String correlationId){
        var ret = ResponseEntity.ok(clientService.getAllClients());
        log.info("Found {} clients", ret.getBody().size());
        return ret;
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<ClientResponse> getClient(@PathVariable Long id) throws Exception {
        ClientResponse client = clientService.getClientById(id);
        log.info("Found client with id: {}", id);
        return ResponseEntity.ok().body(client);
        
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteClient(@PathVariable Long id,
                                             @RequestHeader(value = CORRELATION_ID_HEADER, required = false) String correlationId) {
        clientService.deleteClientById(id);
        log.info("Deleted client with id: {}", id);
        return ResponseEntity.noContent().build();
    }
}