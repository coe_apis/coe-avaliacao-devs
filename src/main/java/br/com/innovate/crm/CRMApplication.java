package br.com.innovate.crm;


import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;



@OpenAPIDefinition(
        info = @Info(
                   title = "Client API",
                description = "API for managing clients",
                version = "1.0",
                termsOfService = "http://localhost:8080/terms",
                contact = @Contact(name = "CoeMS", email = "coe.ms@claro.com.br")
        )
)
@SpringBootApplication
@EnableFeignClients
public class CRMApplication {

    public static void main(String[] args) {
        SpringApplication.run(CRMApplication.class, args);
    }

}