package br.com.innovate.crm.observbility;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public class CustomHealthIndicator implements HealthIndicator {

    private final SessionFactory sessionFactory;

    
    public CustomHealthIndicator(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Health health() {
        boolean healthCheckPassed = checkCustomHealthCondition();

        if (healthCheckPassed) {
            return Health.up().withDetail("Custom Health Check", "Database is reachable").build();
        } else {
            return Health.down().withDetail("Custom Health Check", "Database is not reachable").build();
        }
    }

    private boolean checkCustomHealthCondition() {

    }
}
