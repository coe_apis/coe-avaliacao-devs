package br.com.innovate.crm.clientviacep;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import br.com.innovate.crm.entity.Cep;

/**
 * This interface represents a Feign client for consuming the ViaCEP API to fetch address information based on the given postal code (CEP).
 * The base URL for the ViaCEP service is "https://viacep.com.br/".
 *
 * <p>The ViaCepClient interface provides a method to get the address information for a given CEP in JSON format.</p>
 *
 * <p>Usage example:</p>
 * <pre>{@code
 * @Autowired
 * private ViaCepClient viaCepClient;
 *
 * public void someMethod() {
 *     Cep cepInfo = viaCepClient.getCep("01001000");
 *     // process the cepInfo as needed
 * }
 * }</pre>
 */


public interface ViaCepClient {
    @GetMapping(value = "/ws/{cep}/json")
    public Cep getCep(@PathVariable("cep") String cep);

 }
