package br.com.innovate.crm.model;

public class ClientRequest {
    private String name;
    private String email;
    private String cpf;
    private String cep;

    public ClientRequest() {
    }

    public ClientRequest(String name, String email, String cpf, String cep) {
        this.name = name;
        this.email = email;
        this.cpf = cpf;
        this.cep = cep;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getCpf() {
        return cpf;
    }

    public String getCep() {
        return cep;
    }
}
