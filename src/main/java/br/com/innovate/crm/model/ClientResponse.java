package br.com.innovate.crm.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class ClientResponse {
    private Long id;
    private String name;
    private String cpf;
    private String email;
    private CepResponse cep;

}
