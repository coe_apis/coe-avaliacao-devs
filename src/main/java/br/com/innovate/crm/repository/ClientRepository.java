package br.com.innovate.crm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.innovate.crm.entity.Client;


/**
 * The ClientRepository interface provides the mechanism for storage, retrieval, update,
 * and delete operations on Client objects.
 *
 * <p>This repository extends JpaRepository, which provides JPA-related methods
 * such as saving, deleting, and finding Client entities by their ID.</p>
 */


@Repository
public interface ClientRepository extends JpaRepository<Client,Long>  {
    boolean existsByClientCep_cep(String cep);

    Client findByCpf(String cpf);
}

