package br.com.innovate.crm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.innovate.crm.entity.Cep;

/**
 * The CepRepository interface provides the mechanism for storage, retrieval, update,
 * and delete operations on Cep objects.
 *
 * <p>This repository extends JpaRepository, which provides JPA-related methods
 * such as saving, deleting, and finding Cep entities.</p>
 */
@Repository
public interface CepRepository extends JpaRepository<Cep, String> {

}