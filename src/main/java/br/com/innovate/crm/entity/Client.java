package br.com.innovate.crm.entity;

import jakarta.persistence.*;

/**
 * The Client class represents a client entity in the system.
 * This entity is mapped to the database table "tab_client".
 *
 * <p>The Client class contains information such as the client's name, email, and associated address (CEP).</p>
 *
 * <p>Usage example:</p>
 * <pre>{@code
 * Cep cep = new Cep("01001000", "Praça da Sé", "lado ímpar", "Sé", "São Paulo", "SP", "3550308", "", "11", "7107");
 * Client client = new Client("John Doe", "john.doe@example.com", cep);
 * System.out.println(client);
 * }</pre>
 */

@Entity
@Table(name="tab_client")
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name="name")
    private String name;
    @Column(name="email")
    private String email;

    @Column(name="cpf", unique = true) 
    private String cpf;

    @ManyToOne
    @JoinColumn(name = "cep_id")
    private Cep clientCep;

    public Client() {
    }

    public Client(String name, String email, Cep clientCep, String cpf) {
        this.name = name;
        this.email = email;
        this.clientCep = clientCep;
        this.cpf = cpf;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Cep getClientCep() {
        return clientCep;
    }

    public void setClientCep(Cep clientCep) {
        this.clientCep = clientCep;
    }

    public Long getId() {
        return id;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }


    @Override
    public String toString() {
        return "Client{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", clientCep=" + clientCep +
                '}';
    }
}
