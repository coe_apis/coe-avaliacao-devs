package br.com.innovate.crm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.innovate.crm.entity.Cep;
import br.com.innovate.crm.entity.Client;
import br.com.innovate.crm.model.CepResponse;
import br.com.innovate.crm.model.ClientRequest;
import br.com.innovate.crm.model.ClientResponse;
import br.com.innovate.crm.repository.ClientRepository;
import br.com.innovate.crm.utils.Validators;
import lombok.extern.slf4j.Slf4j;

import java.util.List;


@Service
@Slf4j
public class ClientService {
    private final ClientRepository clientRepository;

    
    private CEPService cepService;

    public ClientService( ClientRepository clientRepository, CEPService cepService) {
        this.clientRepository = clientRepository;
        this.cepService = cepService;
    }

    public ClientResponse saveClientWithCep(ClientRequest clientRequest) {
        String cepNumber = clientRequest.getCep();
        log.info("Verifying if client with CEP {} exists", cepNumber);

        // verify if client cpf is valid
        if (!Validators.isCPFValid(clientRequest.getCpf())) {
            log.error("Invalid CPF: {}", clientRequest.getCpf());
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, "Invalid CPF");
        }

        if (clientRepository.findByCpf(clientRequest.getCpf()) != null) {
            log.error("Client with CPF {} already exists", clientRequest.getCpf());
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Client with CPF already exists");

        }

        Cep cep = cepService.getOrCreateCep(cepNumber);

        var clientToSave = new Client(clientRequest.getName(), clientRequest.getEmail(), cep, clientRequest.getCpf());
        Client savedClient = clientRepository.save(clientToSave);
        
        return mapClientToClientResponse(savedClient);
    }

    private ClientResponse mapClientToClientResponse(Client client) {

        var cepResponse = CepResponse.builder().cep(client.getClientCep().getCep())
                .localidade(client.getClientCep().getLocalidade())
                .bairro(client.getClientCep().getBairro())
                .logradouro(client.getClientCep().getLogradouro())
                .uf(client.getClientCep().getUf()).build();

        return ClientResponse.builder().cep(cepResponse)
                .cpf(client.getCpf())
                .email(client.getEmail())
                .name(client.getName())
                .id(client.getId()).build();

    }

    public List<ClientResponse> getAllClients() {
        var allClients = clientRepository.findAll();

        return allClients.stream().map(client -> {
            return mapClientToClientResponse(client);
        }).toList();
    }

    public ClientResponse getClientById(Long id) {

        var client = clientRepository.findById(id).orElseThrow(() -> {
            log.error("Client with ID {} not found", id);
            return new ResponseStatusException(HttpStatus.NOT_FOUND, "Client not found");
        });

        return mapClientToClientResponse(client);
    }

    public void deleteClientById(Long clientId) {
        clientRepository.deleteById(clientId);
        log.debug("Client with ID {} deleted successfully", clientId);
    }
}
