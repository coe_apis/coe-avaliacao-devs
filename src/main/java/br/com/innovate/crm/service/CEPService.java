package br.com.innovate.crm.service;
import org.springframework.web.server.ResponseStatusException;

import br.com.innovate.crm.clientviacep.ViaCepClient;
import br.com.innovate.crm.entity.Cep;
import br.com.innovate.crm.repository.CepRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;


@Service
@Slf4j
public class CEPService {

    @Autowired
    private CepRepository cepRepository;

    @Autowired
    private ViaCepClient viaCepClient;

    public Cep getOrCreateCep(String cepNumber)
    {

        //validate if cep is valid  with 9 digits nnnnn-nnn
        if (!cepNumber.matches("\\d{5}-\\d{3}"))
        {
            log.error("Invalid CEP: {}", cepNumber);
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, "Invalid CEP");
        }

        Cep existingCep = cepRepository.findById(cepNumber).orElse(null);

        if (existingCep == null) {
            log.info("CEP {} not found in the database. Fetching from ViaCEP API", cepNumber);
            Cep cep = viaCepClient.getCep(cepNumber);

            log.info("Fetched CEP details: {}", cep);
            // save the fetched CEP details to the database
            existingCep = cepRepository.save(cep);
            log.info("Saved CEP details to the database: {}", existingCep);
        } else
        {
            log.info("CEP {} found in the database", cepNumber);
        }

        return existingCep;
            
    }
    
}
