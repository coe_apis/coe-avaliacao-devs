package br.com.innovate.crm.config;

import com.zaxxer.hikari.HikariDataSource;

import lombok.extern.log4j.Log4j2;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class DatabaseConfigLogger implements CommandLineRunner {

    private final HikariDataSource dataSource;

    // Injeta o HikariDataSource configurado pelo Spring Boot
    public DatabaseConfigLogger(@Lazy HikariDataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("-------- HikariCP Connection Pool Properties --------");
        log.info("Datasource URL: " + dataSource.getJdbcUrl());
        log.info("Datasource Username: " + dataSource.getUsername());
        log.info("Minimum Idle Connections: " + dataSource.getMinimumIdle());
        log.info("Maximum Pool Size: " + dataSource.getMaximumPoolSize());
        log.info("Connection Timeout: " + dataSource.getConnectionTimeout() + " ms");
        log.info("Idle Timeout: " + dataSource.getIdleTimeout() + " ms");
        log.info("Max Lifetime: " + dataSource.getMaxLifetime() + " ms");
        log.info("------------------------------------------------------");
    }
}
