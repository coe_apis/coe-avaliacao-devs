package br.com.innovate.crm.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import lombok.extern.log4j.Log4j2;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

@Component
@Log4j2
public class DatabaseInitializer {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @jakarta.annotation.PostConstruct
    public void init() {
        try {

             // Verifica se a tabela já contém registros
             Integer count = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM tab_address", Integer.class);

             if (count != null && count > 0) {
                log.info("Tabela tab_address já contém registros, não será executado o script import.sql");
                 return;  // Não executa o script SQL se já houver registros
             }

            // Carrega o arquivo import.sql do classpath
            ClassPathResource resource = new ClassPathResource("import.sql");
            BufferedReader reader = new BufferedReader(new InputStreamReader(resource.getInputStream(), StandardCharsets.UTF_8));

            StringBuilder sql = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sql.append(line);
                // Verifica se a linha termina com ";" indicando o final de um comando SQL
                if (line.trim().endsWith(";")) {
                    jdbcTemplate.execute(sql.toString());
                    sql.setLength(0);  // Reseta o StringBuilder para o próximo comando
                }
            }
            reader.close();
            log.info("Script import.sql executado com sucesso");

        } catch (Exception e) {
            log.error("Erro ao executar o script import.sql", e);
        
        }
    }
}
