# Prova Prática na TelCom Innovate

A TelCom Innovate, uma empresa de telecomunicações de ponta, é conhecida por oferecer serviços de telecomunicações inovadores e de alta qualidade. Para manter seu padrão de excelência, a empresa desenvolveu um processo de seleção que inclui uma prova prática detalhada.

## Introdução à Prova Prática

### Contexto
Estamos prontos para lançar um novo serviço de gerenciamento no nosso app de CRM. E parte da solução é um serviço RESTful que gerencia informações de clientes. A aplicação foi desenvolvida em Java e Spring Boot. O lançamento do serviço está previsto para as próximas semanas, e precisamos garantir que ele esteja pronto para produção.

Com o lançamento do serviço, esperamos um aumento significativo no número de clientes e transações. Portanto, é fundamental que a aplicação seja capaz de lidar com um grande volume de transações e seja escalável para atender à demanda. Além disso, a aplicação deve ser altamente disponível e confiável.

A área usuária juntamente com a área de BI estima que teremos picos de 300.000 (trezentos mil) requisições por minuto (TPS), durante 10 minutos, e a aplicação deve ser capaz de lidar com esse volume de transações.

Felizmente temos você para nos ajudar a garantir que a aplicação esteja pronta para produção. Vamos começar!

### Objetivo

Nesta prova prática, você será avaliado em uma série de tarefas que refletem atividades comuns no desenvolvimento de software. A seguir, detalhamos o que você precisa fazer e o que esperamos que você demonstre em cada etapa.

A prova esta dividida em duas partes: Programação e Infraestrutura. Mas não se preocupe, você terá tempo suficiente para completar todas as tarefas.

Abaixo temos um exemplo de um diagrama de sequência que mostra como a aplicação deve se comportar quando estiver pronta.

![](images/sequence_save_v2.png)

Na desenho acima, a api externa que a aplicação consome é a [ViaCEP](https://viacep.com.br/). A aplicação consome a api do ViaCEP para buscar informações de endereço a partir do CEP fornecido. Certifique-se de que a aplicação está fazendo a chamada corretamente.

Antes vamos fazer alguns ajustes no ambiente de desenvolvimento que já foi configurado anteriormente.

### Namespace Kubernetes
Vamos utilizar o namespace `coe-avaliacao` para realizar a prova. Crie-o no seu Cluster Kubernetes.

### Banco de Dados.
A aplicação utiliza um banco de dados PostgreSQL. A aplicação faz todas as configurações necessárias (criação de tabelas, inserção de dados iniciais, etc) automaticamente. Você só precisa configurar o banco de dados, alterar os parametro de conexão na aplicação e a ela irá fazer o resto.

Caso não tenha um banco de dados PostgreSQL instalado, você pode aplicar o arquivo 
[postgres_pgadmin_k8s.yaml](../Documents/criacao_de_ambientes/postgres_pgadmin_k8s.yaml)

Esse deployment irá criar um banco de dados PostgreSQL e uma interface pgAdmin4 para gerenciamento do banco de dados. Para o banco de dados, são criados dois services, um do tipo ClusterIP e outro do tipo NodePort. O serviço do tipo NodePort é utilizado para acessar o banco de dados externamente. O serviço do tipo ClusterIP é utilizado para acessar o banco de dados internamente.

O PgAdmin é uma interface web para gerenciamento de banco de dados PostgreSQL. Ele expõe um ingress e você de configurar o `host`usando o serviço [nip.io](nip.io) para criar domínios personalizados de forma rápida e fácil.

### Requistos não funcionais
A aplicação deve rodar em um ambiente Kubernetes, com os seguintes recursos:
- **CPU:** 3
- **Memória:** 4GB
- **Minimo de replicas:** 2
- **Autoescalamento:** Deve ser configurado para aumentar o número de réplicas conforme a necessidade. (HPA)
- **Throughput:** O pico de transações esperado na nossa aplicação é de 300.000 (trezendos mil) requisições por por minuto (TPS).

## Tarefas da Prova
### Parte 1: Programação (Java)
A aplicação é um serviço RESTful que gerencia informações de clientes. A aplicação foi desenvolvida em Java e Spring Boot. A aplicação deve possuir métodos para criar, buscar e deletar clientes, além de métodos para validar CPF, CEP e Email. Abra o  [swagger](../Documents/SwaggerCRMInnovate.json) da aplicação para verificar os métodos que devem estar disponíveis. Você pode usar o [Swagger Editor Online](https://editor.swagger.io) para visualizar o arquivo.


#### 1. Clonagem do Repositório e Criação de Branch
- Acesse o repositório fornecido pela TelCom Innovate. (https://gitlab.com/coe_apis/coe-avaliacao-dev) 
- Siga as instruções fornecidas no documento [gitflow.md](../Documents/gitflow.md)

#### 2. Compilação da Aplicação
- Retire os erros de compilação da aplicação corrigindo eventuais problemas no código e realizando os imports necessários.


#### 3. Configuração do Postgres
- Altere as configurações de conexão com o banco de dados para que a aplicação se conecte ao banco de dados PostgreSQL. A aplicação é capaz de criar as tabelas automaticamente.

#### 4. Refatoração e Criação de Métodos

- Atualmente a aplicação faz as validações de CEP (Verifica se o CEP fornecido é válido e busca as informações correspondentes) e CPF (Verifica se o CPF fornecido é válido e se já existe no sistema), é necessário `criar` um validação de E-mail, que ira verificar se o formato do email é válido e se já existe no sistema.

- Crie um método PUT para atualizar as informações de um cliente. O método deve:
    - Buscar o cliente pelo ID e lançar uma exceção com status 404 se não for encontrado.
    - Validar o CPF, lançar uma exceção com status 422 se o CPF for inválido, e com status 409 se já houver um cliente com o mesmo CPF (que não seja o cliente atual). Se o CPF for válido e único, atualize-o.
    - Atualizar o nome e o email, caso os valores fornecidos não sejam nulos ou vazios.
    - Atualizar o CEP se for diferente do atual, utilizando um serviço para buscar ou criar o CEP.
    - Salvar o cliente atualizado e retornar a resposta mapeada.

- Lembre-se de validar se estamos consumindo a API do ViaCEP corretamente.

-  Conforme falamos, a aplicação precisa ser resiliente, portanto, é necessário criar um método para verificar a saúde da aplicação. 
   -  Utilize o Actuator para adicionar endpoints de monitoramento e gerenciamento à sua aplicação. 
   -  Corrija para que a aplicação retorne o status `UP` quando a conexão com o banco de dados estiver ativa e `DOWN` quando a conexão com o banco de dados estiver inativa.
   -  Configure as Probes nos artefatos Kubernetes para garantir que o Kubernetes monitore a saúde da aplicação utilizando os endpoints do Actuator.

#### 5. Formato do Log
No ambiente de produção é fundamental que os logs sejam registrados em um formato padronizado pois utilizamos a suite ELK para agregação dos Logs. Os logs devem ser registrados no formato `JSON` para facilitar a análise e a integração com ferramentas de monitoramento e análise de logs.

- Atualize a aplicação para que os logs sejam registrados no formato JSON. 

#### 6. Cobertura de Testes Unitários
No desenvolvimento de software, é fundamental garantir que o código seja testado de forma adequada. A cobertura de testes é uma métrica que indica a porcentagem de código que é coberta por testes unitários. Uma cobertura de testes acima de 80% é considerada um bom padrão de qualidade e é o mínimo aceitável na TelCom Innovate, inclusive é verificado no pipeline de CI/CD.

- Garanta que a cobertura de testes unitários esteja acima de **80%**. Utilize um plugin como o _EclEmma_ para **Eclipse** ou _Extension Pack for Java_ para **VsCode** , para visualizar a cobertura de código.
- Corrija os testes existentes, se necessário e crie testes para os métodos desenvolvidos.

##### Como testar a aplicação (ambiente de desenvolvimento)
  Você pode utilizar o Swagger para testar a aplicação, ou se preferir, utilize o Postman, Curl ou qualquer outra ferramenta de sua preferência. Para o postman, você pode importar o arquivo [Prova.postman_collection.json](../Documents/Prova.postman_collection.json) e utilizar as chamadas já configuradas.

### Parte 2: Infraestrutura (Kubernetes)

Agora que a aplicação está pronta, é hora de configurar o ambiente de produção no Kubernetes. Nesta etapa, vamos criar a imagem do Docker, configurar o ambiente Kubernetes e realizar um teste de estresse para determinar a configuração ideal dos recursos.

#### 1. Docker
- Compile a aplicação, ajuste o Dockerfile conforme necessário e faça o Build da imagem localmente. Lembrando que o desenvolvedor anterior não sabe dizer se todos os ajustes necessários foram feitos.
- Coloque a tag da imagem com o seu nome de usuário do Docker Hub e faça o push da imagem para o Docker Hub.

#### 2. Teste de Estresse 
Para saber o quanto a aplicação é capaz de suportar, realize um teste de estresse na aplicação. O teste de estresse deve ser capaz de determinar o pico de transações que a aplicação pode suportar, bem como a latência de resposta, a taxa de erros e o throughput. No entanto, apontando o teste de stress para o seu ambiente de desevolvimento vai ser possível determinar a configuração **`inicial`** dos recursos do Kubernetes. Você deve repetir o teste de stress apontando para o ambiente do kubernetes para determinar a configuração **`ideal`** dos recursos.

- Realize um teste de estresse na aplicação para determinar a configuração ideal dos recursos do Kubernetes, respeitando os limites determinados no documento “criação de ambientes” (3 CPU e 4 GB de Memória).
- O arquivo jmx do teste de estresse já se encontra no diretório [stress_test_plan.jmx](../Documents/stress_test_plan.jmx), é necessário apenas ajustar o IP do seu ambiente e executar o teste e os caminhos dos arquivos csv's ([clientes.csv](../Documents/clientes.csv) e [clientes_novos.csv](../Documents/clientes_novos.csv)).

### 3. Kubernetes
Nosso ambiente de produção é baseado em Kubernetes. A aplicação deve ser implantada no Kubernetes e configurada para atender aos requisitos de escalabilidade, disponibilidade e confiabilidade. Lembre-se de que a aplicação deve ser capaz de lidar com um grande volume de transações e ser escalável para atender à demanda.

O Kubernetes é uma plataforma de orquestração de contêineres que automatiza a implantação, o dimensionamento e a operação de aplicativos em contêineres. O Kubernetes é amplamente utilizado na TelCom Innovate para implantar aplicativos em ambientes de produção.

Os artefatos kubernetes estão localizados no diretório [K8s](../K8s). Como estamos no ambiente de desenvolvimento, você deve ajustar os valores nas pastas `dev` e `base` conforme necessário. Essa abordagem utiliza o [Kustomize](https://kustomize.io) para gerar os artefatos finais.

Abaixo temos uma lista das pendências que o desenvolvedor anterior deixou de presente para nós e que devemos resolver. No entanto, caso necessário, você pode adicionar ou alterar mais artefatos para atender aos requisitos da aplicação.

#### Artefatos
- **Deployment**:
Lembre-se que o deployment é responsável por garantir que a aplicação esteja sempre disponível e em execução. O deployment deve ser configurado para atender aos requisitos de escalabilidade, disponibilidade e confiabilidade. Estragias de Rolling Update e Rollback devem garantir que não haja interrupção no serviço.

  - _RECURSOS_: Utilize as informações encontradas no teste de estresse para preencher adequadamente os valores `iniciais` dos recursos (CPU e Memória) para o deployment.
  - _PROBES_: A TelCom Innovate utiliza o Actuator para adicionar endpoints de monitoramento e gerenciamento à sua aplicação. Configure as Probes nos artefatos Kubernetes para garantir que o Kubernetes monitore a saúde da aplicação utilizando os endpoints do Actuator.
  - _IMAGEM_: Para que o deploy seja executado corretamente, configure adequadamente os campos de imagem no artefato Kubernetes.

- **HPA:** Levando em consideração os padrões da TelCom Innovate, onde o mínimo de réplicas deve ser 2, e os valores de minReplica e maxReplica devem ser definidos de acordo com o teste de estresse, preencha esses valores de acordo com o resultado encontrado.
- **ConfigMap:** Exponha os endpoints de monitoramento da saúde da aplicação dentro dos artefatos Kubernetes.
  - _ENDPOINTS_: Exponha os endpoints de monitoramento da saúde da aplicação dentro dos artefatos Kubernetes.
  - _POOL DE CONEXÕES_: A aplicação utiliza o hikari para gerenciar o pool de conexões com o banco de dados. Configure o pool de conexões no ConfigMap para garantir que a aplicação possa lidar com um grande volume de transações. Em testes anteriores foi identificado que ajustes no pool de conexão (aumentando ou diminuindo) pode proporcionar um ganho de performance de 3 a 4 vezes em relação as configurações padrões. Só tome cuidado pois o pool de conexões não pode ser muito grande, pois pode causar problemas de performance no banco de dados com o saturamento de conexões. 
  
- **Secret:** No Kubernetes, um Secret pode ser utilizado para armazenar credenciais de forma segura. Este Secret é referenciado no Ingress para configurar a autenticação básica, garantindo que apenas usuários autenticados possam acessar os serviços protegidos. Utilize o padrão da TelCom Innovate (user e pass criado via .[htpasswd](https://www.web2generators.com/apache-tools/htpasswd-generator) e encriptado em Base64) para preencher este artefato.

- **Ingress:** Realize as configurações do Ingress de acordo com os Secrets configurados.
  - _HOST_: Utilize o serviço [nip.io](nip.io) para criar domínios personalizados de forma rápida e fácil.
  - _PORT, PATH e Prefix_:  assegure que os valores estejam corretos de acordo com que é exposto pela aplicação.

### 4. Análise Final

- No final da prova, a aplicação será analisada para garantir que está compilando corretamente e retornando os logs no formato JSON.
-  Execute os testes apontandos para o ambiente do Kubernetes e verifique se a aplicação está funcionando corretamente.
- Explique como foram encontrados os valores dos recursos utilizados nos artefatos Kubernetes (a partir do teste de estresse).
- Suba as alterações para o repositório GitLab e crie um Merge Request para a branch `master`. Compartilhe conosco o link do MR.

#### Dicas importantes
- Posso usar o Bing para pesquisar?
  - Sim, você pode usar o Bing para pesquisar.
  
- Posso usar o chatgpt, gemini, blackbox ou qualquer outra GenIA?
  - Sim, mas cuidado com a resposta, você precisa saber explicar o que foi feito.
  
-  Posso usar o Baeldung, StackOverflow, etc para copiar o código?
   - Sim, se o código estiver lá, maravilha, mas você precisa saber explicar o que foi feito.
  
- Posso pedir ajuda a um amigo?
  - Não, você deve realizar a prova sozinho. O intuito é avaliar suas habilidades e conhecimentos para nos ajudar justamente levantar quais pontos precisamos focar em treinamentos futuros.

## Boa Sorte!

Esta prova prática é projetada para desafiar suas habilidades e demonstrar sua capacidade de resolver problemas em um ambiente de desenvolvimento de software. Boa sorte!
