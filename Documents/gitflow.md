## GitFlow

- Crie um fork do repositório no GitLab. (Esse repositório deve ser público)
- Clone o repositório forkado localmente.
- Crie uma branch seguindo o git flow para fazer as correções necessárias.
- Realize as correções e desenvolvimentos necessários no código.

